module.exports = (express, controller) => {
    const router = express.Router();
    var cors = require('cors');
  
    router.get('/', controller.getAll);
  
   // router.get('/:id', controller.getOne);
   router.get('/:idPartie', cors(), controller.getOne);
  
    router.post('/', cors(), controller.create);
  
    router.put('/rejoindre/:idPartie', cors(), controller.rejoindre);

    router.put('/updateSigne/:idPartie', cors(), controller.updateSigne);

    router.put('/:id', controller.update);
  
    router.delete('/:id', controller.delete);
  
    return router;
  };
  