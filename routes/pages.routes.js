module.exports = (express, controller) => {
  const router = express.Router();

  router.get('/', controller.home);

  return router;
};
