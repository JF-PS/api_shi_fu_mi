module.exports = (express, controller) => {
  const router = express.Router();
  var cors = require('cors');

  router.get('/', controller.getAll);

  router.get('/:idUser', controller.getOne);

  router.get('/:login/:password', cors(), controller.logIn);

  router.post('/', cors(), controller.create);

  router.put('/:id', controller.update);

  router.delete('/:id', controller.delete);

  return router;
};
