const mongoose = require('mongoose');

const partieSchema = mongoose.Schema({
  idPartie: String,
  idUser1: String,
  idUser2: String,
  signeUser1: String,
  signeUser2: String,
  nbManche: String,
  mancheGagnéUser1: String,
  mancheGagnéUser2: String
}, {
  timestamps: true
});

module.exports = mongoose.model('Partie', partieSchema);
