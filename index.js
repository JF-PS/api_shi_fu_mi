const express = require('express');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
var cors = require('cors');
const mongoose = require('mongoose');
const env = require('./env.json');
require('colors');


const UsersRepository = require('./repositories/users.mongo');
const PartiesRepository = require('./repositories/parties.mongo');
const pagesController = require('./controllers/pages.controller');
const usersController = require('./controllers/users.controller');
const partiesController = require('./controllers/parties.controller');
const pagesRoutes = require('./routes/pages.routes');
const usersRoutes = require('./routes/users.routes');
const partiesRoutes = require('./routes/parties.route');
const usersRepository = new UsersRepository();
const partiesRepository = new PartiesRepository();
const app = express();
const port = 4000;


mongoose.connect('mongodb+srv://shi-fu-mi.6zldz.mongodb.net', {
  user: env.DB_USER,
  pass: env.DB_PASS,
  dbName: 'shi-fu-mi',
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
},
(error) => {
  if (error) throw error;
  console.log('MongoDB server connected');
});

app.use(bodyParser.urlencoded({
  extended: false
}));

app.use(bodyParser.json());

app.use(cors());

app.engine('.hbs', exphbs({ extname: '.hbs' }));
app.set('view engine', '.hbs');

app.use('/api/users', usersRoutes(express, usersController(usersRepository)));
app.use('/', pagesRoutes(express, pagesController(usersRepository)));
app.use('/api/parties', partiesRoutes(express, partiesController(partiesRepository)));

app.use((req, res, next) => {
  res.json({ status: 'error detected' });
});

app.listen(port, () => {
  console.log(`Adresse du serveur: http://localhost:${port}`.green.bold);
});
