const Partie = require('../models/partie.model');

module.exports = class PartiesRepository {

  getAll() {
    return new Promise((resolve, reject) => {
      Partie.find((err, parties) => {
        err ? reject(err) : resolve(parties.map((partie) => partie.toObject()));
      });
    });
  }

   getOne(id) {
     return new Promise((resolve, reject) => {
       Partie.findById(id, (err, partie) => {
         err ? reject(err) : resolve(partie.toObject());
       });
     });
   }

/*  getOne(idPartie) {
    return new Promise((resolve, reject) => {
      Partie.findOne({idPartie: idPartie}, (err, partie) => {
        if(partie==null){
          resolve({result: "No partie found"})
        }
        else{
          err ? reject(err) : resolve(partie.toObject());
          console.log(partie.toObject().idPartie)
        }

      });
    });
  }*/

  create({ idPartie, idUser1 }) {
    return new Promise((resolve, reject) => {
        Partie.create({ idPartie, idUser1, idUser2: "" }, (err, partie) => {
            err ? reject(err) : resolve(partie.toObject());
        });
    });
  }

  rejoindre(idPartie, attributes){
    return new Promise((resolve, reject) => {
      Partie.findByIdAndUpdate(idPartie, attributes, (err, partie) => {
        Partie.findById(idPartie, (err, p) => {
          err ? reject(err) : resolve(p.toObject());
        });
      });
    });
  }

  updateSigne(idPartie, attributes){
    return new Promise((resolve, reject) => {
      Partie.findByIdAndUpdate(idPartie, attributes, (err, partie) => {
        Partie.findById(idPartie, (err, p) => {
          err ? reject(err) : resolve(p.toObject());
        });
      });
    });
  }


  

  delete(id) {
    return new Promise((resolve, reject) => {
      User.findByIdAndRemove(id, (err, user) => {
        err ? reject(err) : resolve(user.toObject());
      });
    });
  }
}
