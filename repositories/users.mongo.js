const User = require('../models/user.model');

module.exports = class UsersRepository {

  getAll() {
    return new Promise((resolve, reject) => {
      User.find((err, users) => {
        err ? reject(err) : resolve(users.map((user) => user.toObject()));
      });
    });
  }

   getOne(id) {
     return new Promise((resolve, reject) => {
       User.findById(id, (err, user) => {
         err ? reject(err) : resolve(user.toObject());
       });
     });
   }

  logIn(login, password) {
    return new Promise((resolve, reject) => {
      User.findOne({login: login, password: password}, (err, user) => {
        if(user==null){
          resolve({result: "No user found"})
        }
        else{
          err ? reject(err) : resolve(user.toObject());
        }

      });
    });
  }

  getLogin(login){
    return new Promise((resolve, reject) => {
      User.findOne({login: login}, (err, user) => {
        if(user!=null){
          resolve({result: "This login already exist"}).toObject();
        }
        else{ 
          err ? reject(err) : resolve(user.toObject());
        }
      })
    })
  }

  create({ login, password }) {
    return new Promise((resolve, reject) => {
      var res = this.getLogin(login);
      if( res == {result: "This login already exist"}){
        alert("This login already exist");
        reject(err);
      }
      else{
        User.create({ login, password }, (err, user) => {
            err ? reject(err) : resolve(user.toObject());
        });
      }

    });
  }


  update(id, attributes) {
    return new Promise((resolve, reject) => {
      User.findByIdAndUpdate(id, attributes, (err, user) => {
        err ? reject(err) : resolve(user.toObject());
      });
    });
  }

  delete(id) {
    return new Promise((resolve, reject) => {
      User.findByIdAndRemove(id, (err, user) => {
        err ? reject(err) : resolve(user.toObject());
      });
    });
  }
}
