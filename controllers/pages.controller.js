module.exports = (repository) => ({

  home(req, res) {
    repository.getAll().then((users) => {
      res.render('home', { users });
    });
  }
});
