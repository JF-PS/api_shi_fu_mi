module.exports = (repository) => ({

    getAll(req, res) {
      repository.getAll().then((parties) => {
        res.json(parties);
      });
    },
  
    // getOne(req, res) {
    //   repository.getOne(req.params.id).then((found) => {
    //     if (found) {
    //       res.json(found);
    //     } else {
    //       res.sendStatus(404);
    //     }
    //   });
    // },
    
    getOne(req, res) {
      repository.getOne(req.params.idPartie).then((found) => {
        if (found) {
          res.json(found);
        } else {
          res.sendStatus(404);
        }
      });
    },
  
    create(req, res) {
      repository.create(req.body).then((created) => {
        if (created) {
          res.json(created);
        } else {
          res.sendStatus(500);
        }
      });
    },

    rejoindre(req, res){
        repository.rejoindre(req.params.idPartie, req.body).then((partie)=>{
            res.json(partie);
        }).catch((err) =>{
            console.log(err);
            res.sendStatus(500);
        })
    },

    updateSigne(req, res){
        repository.updateSigne(req.params.idPartie, req.body).then((partie)=>{
            res.json(partie);
        }).catch((err) =>{
            console.log(err);
            res.sendStatus(500);
        })
    },

    update(req, res) {
      repository.update(req.params.id, req.body).then((user) => {
        res.json(user);
      }).catch((err) => {
        res.sendStatus(500);
      });
    },
  
    delete(req, res) {
      repository.delete(req.params.id).then(() => {
        res.sendStatus(200);
      });
    }
  });