module.exports = (repository) => ({

  getAll(req, res) {
    repository.getAll().then((users) => {
      res.json(users);
    });
  },

   getOne(req, res) {
    console.log("user id : " + req.params.idUser);
     repository.getOne(req.params.idUser).then((found) => {
       if (found) {
         res.json(found);
       } else {
         res.sendStatus(404);
       }
     });
   },
  
  logIn(req, res) {
    repository.logIn(req.params.login, req.params.password).then((found) => {
      if (found) {
        res.json(found);
      } else {
        res.sendStatus(404);
      }
    });
  },

  create(req, res) {
    repository.create(req.body).then((created) => {
      if (created) {
        res.json(created);
      } else {
        res.sendStatus(500);
      }
    });
  },

  update(req, res) {
    repository.update(req.params.id, req.body).then((user) => {
      res.json(user);
    }).catch((err) => {
      res.sendStatus(500);
    });
  },

  delete(req, res) {
    repository.delete(req.params.id).then(() => {
      res.sendStatus(200);
    });
  }
});